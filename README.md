------------------------------------------
        Never Ending Crypto Redux
        
The challenge is back and stronger than ever!  
  
IT WILL NEVER END!!  

Good luck. You're gonna need it.


### Walkthrough:

Cipher1 -> Cipher2 indicates applying Cipher 1 then Cipher 2.

Matrix Cipher:
p = ABCDEFGHIJKLMNOPQRSTUVWXYZ

| 1 | 2 | 3 |
|---|---|---| 
| A | B | C |
| D | E | F |
| G | H | I |
| J | K | L |
| M | N | O |
| P | Q | R |
| S | T | U |
| V | X | X |
| Y | Z |   |

c = ADGJMPSVYBEHKNQTXZCFILORUX

FlipMorse just flips the Morse Code: -.-. -> .-.-

The others are self-explanatory.

Level 0:

Morse

Level 1:

Rot13 -> Morse

Level 2:

Morse -> FlipMorse

Level 3:

Matrix -> Morse

Level 4:

Matrix -> Rot13 -> Morse

Level 5:

Matrix -> Rot13 -> Morse -> FlipMorse

Level 6:

Vigenere(TUCTF) -> Morse

Level 7:

Vigenere(HAVEFUN) -> Matrix -> Morse -> FlipMorse


